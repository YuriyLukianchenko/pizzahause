package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

public class OnionPizza extends Pizza {

    @Override
    public void prepare() {
        this.dough = DoughType.THICK;
        this.sauce = SauceType.MARINARA;
        this.toppings.add(Topping.MEAT);
        this.toppings.add(Topping.ONION);
    }
}

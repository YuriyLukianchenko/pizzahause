package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

public class FruitPizza extends Pizza{
    @Override
    public void prepare() {
        this.dough = DoughType.THIN;
        this.sauce = SauceType.PLUM_TOMATO;
        this.toppings.add(Topping.FRUIT);
        this.toppings.add(Topping.PARMESAN);
    }

}

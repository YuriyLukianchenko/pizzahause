package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

import java.util.ArrayList;
import java.util.List;

public abstract class Pizza {
    protected DoughType dough;
    protected SauceType sauce;
    protected List<Topping> toppings;

    public Pizza(){
        this.toppings = new ArrayList<>();
    }
    public abstract void prepare();
    public void bake(){
        // pizza was backed
    }
    public void cut(){
        //pizza was cut
    }
    public void box(){
        // pizza was boxed
    }

    public DoughType getDough() {
        return dough;
    }

    public void setDough(DoughType dough) {
        this.dough = dough;
    }

    public SauceType getSauce() {
        return sauce;
    }

    public void setSauce(SauceType sauce) {
        this.sauce = sauce;
    }

    public List<Topping> getToppings() {
        return toppings;
    }

    public void setToppings(List<Topping> toppings) {
        this.toppings = toppings;
    }

    @Override
    public String toString(){
        return "Pizzas components: dough= " + dough + ", sauce= " + sauce + ", toppings= " + toppings + ".";
    }
}

package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

public class DoubleCheesePizza extends Pizza {
    @Override
    public void prepare() {
        this.dough = DoughType.THICK;
        this.sauce = SauceType.MARINARA;
        this.toppings.add(Topping.TOMATO);
        this.toppings.add(Topping.ROCKFORD);
    }
}

package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.DoughType;
import com.lukianchenko.model.ingridientsenum.SauceType;
import com.lukianchenko.model.ingridientsenum.Topping;

public class DoubleRockfordPizza extends Pizza {
    @Override
    public void prepare() {
        this.dough = DoughType.THIN;
        this.sauce = SauceType.PLUM_TOMATO;
        this.toppings.add(Topping.MEAT);
        this.toppings.add(Topping.ONION);
        this.toppings.add(Topping.ROCKFORD);
        this.toppings.add(Topping.ROCKFORD);
    }
}

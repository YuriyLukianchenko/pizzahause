package com.lukianchenko.model.ingridientsenum;

public enum DoughType {
    THICK, THIN
}

package com.lukianchenko.model.ingridientsenum;

public enum SauceType {
    MARINARA, PLUM_TOMATO
}

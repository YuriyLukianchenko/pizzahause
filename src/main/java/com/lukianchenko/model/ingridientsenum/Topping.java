package com.lukianchenko.model.ingridientsenum;

public enum Topping {
    ROCKFORD, PARMESAN, ONION, FRUIT, MEAT, TOMATO
}

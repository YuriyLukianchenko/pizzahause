package com.lukianchenko.model.factories;

import com.lukianchenko.model.ingridientsenum.PizzaType;
import com.lukianchenko.model.pizzas.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class KyivPizzaFactory extends PizzaFactory {
    Logger logger = LogManager.getLogger(LvivPizzaFactory.class);
    @Override
    protected Pizza createPizza(PizzaType type) {
        switch (type){
            case CHEESE:
                return new DoubleRockfordPizza();
            case VEGGIE:
                return new FruitPizza();
            default:
                logger.info("unknown type of Pizza");
                return null;
        }
    }
}

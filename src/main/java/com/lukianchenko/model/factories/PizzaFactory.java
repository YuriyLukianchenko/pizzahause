package com.lukianchenko.model.factories;

import com.lukianchenko.model.ingridientsenum.PizzaType;
import com.lukianchenko.model.pizzas.Pizza;

public abstract class PizzaFactory {
    public Pizza orderPizza(PizzaType type) {
        return createPizza(type);
    }
    protected abstract Pizza createPizza(PizzaType type);
}

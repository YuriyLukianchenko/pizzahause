package com.lukianchenko.model.factories;

import com.lukianchenko.model.ingridientsenum.PizzaType;
import com.lukianchenko.model.pizzas.DoubleCheesePizza;
import com.lukianchenko.model.pizzas.OnionPizza;
import com.lukianchenko.model.pizzas.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class LvivPizzaFactory extends PizzaFactory {
    Logger logger = LogManager.getLogger(LvivPizzaFactory.class);
    @Override
    protected Pizza createPizza(PizzaType type) {
        switch (type){
            case CHEESE:
                return new DoubleCheesePizza();
            case VEGGIE:
                return new OnionPizza();
            default:
                logger.info("unknown type of Pizza");
                return null;
        }
    }
}

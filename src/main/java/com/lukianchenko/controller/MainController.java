package com.lukianchenko.controller;

import com.lukianchenko.model.factories.KyivPizzaFactory;
import com.lukianchenko.model.factories.LvivPizzaFactory;
import com.lukianchenko.model.factories.PizzaFactory;
import com.lukianchenko.model.ingridientsenum.PizzaType;
import com.lukianchenko.model.pizzas.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class MainController {
    public void start () {
        Logger logger = LogManager.getLogger(MainController.class);
        //Demonstration of different pizza creation by different PizzaFactories
        // Factory Method was used
        List<Pizza> pizzas = new ArrayList<>();
        PizzaFactory pizzaFactory1 = new LvivPizzaFactory();
        pizzas.add(pizzaFactory1.orderPizza(PizzaType.CHEESE));
        pizzas.add(pizzaFactory1.orderPizza(PizzaType.VEGGIE));

        PizzaFactory pizzaFactory2 = new KyivPizzaFactory();
        pizzas.add(pizzaFactory2.orderPizza(PizzaType.CHEESE));
        pizzas.add(pizzaFactory2.orderPizza(PizzaType.VEGGIE));

        pizzas.stream().peek(x->{x.prepare();
            x.bake();
            x.cut();
            x.box(); })
                .forEach(logger::info);
    }
}

package com.lukianchenko;

import com.lukianchenko.controller.MainController;

public class PizzaHauseApplication {
    public static void main(String[] args) {
        MainController mainController = new MainController();
        mainController.start();
    }
}

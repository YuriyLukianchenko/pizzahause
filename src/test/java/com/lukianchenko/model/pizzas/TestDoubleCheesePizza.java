package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.Topping;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestDoubleCheesePizza {
    @Test
    public void testPrepare(){
        Pizza pizza = new DoubleCheesePizza();
        pizza.prepare();
        List<Topping> toppings = pizza.getToppings();
        assertTrue(toppings.contains(Topping.ROCKFORD));

    }
}

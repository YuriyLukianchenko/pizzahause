package com.lukianchenko.model.pizzas;

import com.lukianchenko.model.ingridientsenum.Topping;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class DoubleRockfordPizzaTest {
    @Test
    public void prepareTest(){
        Pizza pizza = new DoubleRockfordPizza();
        pizza.prepare();
        List<Topping> toppings = pizza.getToppings();
        assertTrue(toppings.contains(Topping.ONION));
    }
}
